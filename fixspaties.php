<?php
header('content-type: text/plain');

//Load the file
$thefile=file_get_contents('en.po');

//Get the end of the file
$fileend=substr($thefile,(strrpos($thefile,'"')+1));

//Chop off the trailing whitespaces
$thefile=trim($thefile);

//Make an array to contain the new file
$thenewfile=array();

//Get every id-string tuple as a single array-cell
$exploded=explode("\n\n", $thefile);

//Add the fileheader to the newfile
$thenewfile[]=$exploded[0]."\n";

//Function to fix id-string tupples
function fixtuple($tuple)
{
    $firstCharIn=strpos($tuple[0],'"')+1; //Determine the first actual id-string character
    $firstCharOut=strpos($tuple[1],'"')+1; //Determine the first actual translations-string character
    
    if($tuple[1]=='msgstr ""'){return ($tuple);} //Check if the msgstr is not just empty. If it is, just return it and don't go messing up the translation status
    
	//Check and fix the beginning, if it actually is the first input string
	if(!isset($tuple['skipbegin']))
	{
    	if(substr($tuple[0],$firstCharIn,1)==" " && substr($tuple[1],$firstCharOut,1)!=" ")
    	{
        	$tuple[1]=substr($tuple[1],0,$firstCharOut)." ".substr($tuple[1],$firstCharOut);
    	}
    	elseif(substr($tuple[0],$firstCharIn,1)!=" " && substr($tuple[1],$firstCharOut,1)==" ")
    	{
        	$tuple[1]=substr_replace($tuple[1],"",$firstCharOut,1);
    	}
	}
    	
    
    //Check the end
    if(substr($tuple[0],-2,1)==" " && substr($tuple[1],-2,1)!=" ")
    {
        $tuple[1]=substr($tuple[1],0,-1).' "';
    }
    elseif(substr($tuple[0],-2,1)!=" " && substr($tuple[1],-2,1)==" ")
    {
        $tuple[1]=substr_replace($tuple[1],"",-2,1);
    }    
    //While you're at it, might as well check for periods at the end of strings and fix those too.
	elseif(substr($tuple[0],-2,1)!="." && substr($tuple[1],-2,1)==".")
    {
        $tuple[1]=substr_replace($tuple[1],"",-2,1);
    }
	elseif(substr($tuple[0],-2,1)=="." && substr($tuple[1],-2,1)!=".")
    {
        $tuple[1]=substr($tuple[1],0,-1).'."';
    }        
	//Return the fixed tuple
    return($tuple);
}

//Go through the entire file
for($i=1;$i<count($exploded);$i++)
{
    //Create an array containing the both values of the tuple as seperate values
	$tuple=explode("\n",$exploded[$i]);
    
    //So it's a single line one. That's good...
    if(count($tuple)==2)
    {
        //*Sings* take it eeeeaaaaheeeaaasyyyy!
        $tuple=fixtuple($tuple);
        $thenewfile[]=$tuple[0];
        $thenewfile[]=$tuple[1]."\n";
        
    }
    //Ok, probably multiline...
    else
    {
        //Variables to keep track of where msgid and msgstr start and end
        $firstin=0;
        $lastin=0;
        $firstout=0;
        $lastout=count($tuple)-1;
        
        //Some counters and booleans to help in keeping track
        $a=0;
        $multiline=false;
        $done=false;
        
        //Check every part of the tuple
        foreach($tuple as $line)
        {
            //Comments can be added without further attention
            if(substr($line,0,1)=="#")
            {
                $thenewfile[]=$line;
            }
            //So it's not a comment... Oh well, time to get down and dirty
            else
            {
                //Check if it's something multiline
                if($line=='msgid ""')
                {
                    $firstin=$a+1;
                    $multiline=true;
                }
                elseif($line=='msgstr ""')
                {
                    $firstout=$a+1;
                    $lastin=$a-1;
                }
                //Yay, it's single line. That makes life easy!
                elseif(!$multiline)
                {
                    $in=$a;
                    $out=$a+1;
                 
                    $tuple=fixtuple(array($tuple[$in],$tuple[$out]));
                    $thenewfile[]=$tuple[0];
                    $thenewfile[]=$tuple[1]."\n";
                    $done=true; //Variable to break out of the foreach, because I'm done and don't need to do anything with the next step
                }
            }
            if($done){break;}
            $a++;
        }
        

        if($multiline)
        {
			//Check for nasty multilines where the msgstr is a single line and the and the msgid is a multiline
        	if($firstout != ($tuplesize-2) && $firstin>$firstout)
        	{
        		//Fix the identifying array keys
        		$firstout=$lastout;
        		$lastin=$firstout-1;
        	}
			
			//Actually start doing something with the multiline
			       
			//Check the start of the tuple
        	$tmptuple=fixtuple(array($tuple[$firstin],$tuple[$firstout]));
        	$tuple[$firstin]=$tmptuple[0];
        	$tuple[$firstout]=$tmptuple[1];

        	//Check the end of the tuple
        	$tmptuple=fixtuple(array($tuple[$lastin],$tuple[$lastout], 'skipbegin'=>true));
        	$tuple[$lastin]=$tmptuple[0];
        	$tuple[$lastout]=$tmptuple[1];

        	//Add the fixed multiline tuple to the new file
			$tuplesize=count($tuple);
			
            for($b=($firstin-1);$b<$tuplesize;$b++)
            {
                if($b==($tuplesize-1))
                {
                    $thenewfile[]=$tuple[$b]."\n";
                }
                else
                {
                    $thenewfile[]=$tuple[$b];
                }
            }
        $multiline=false;
        }
        
    }
}

//Append the end of the original file
$thenewfile[]=$fileend;

//Write the reworked contents to the file again.
if(file_put_contents("en.po",implode("\n",$thenewfile)))
{
	print "Fixed your file! Can haz hugz? (or beerz?)\n";
}
else
{
	print "FAIL! Unfortunately I made a royal fuckup when writing to the file.\n Sorry for that, try again when the script has been fixed or the access rights for the file have been correctly set\n";
}


?> 
